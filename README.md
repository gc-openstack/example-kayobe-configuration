Example Graphcore Kayobe Configuration
======================================

This repo contains example configuration for deploying an Openstack using [Kayobe](https://docs.openstack.org/kayobe/latest/).    This is a subset of the [Kayobe Victoria](https://opendev.org/openstack/kayobe-config) configuration.   

The intention is that this configuration is either:
* overlaid onto the kayobe config template for your own deployment
* as a configuration reference (eg for an existing Kolla-Ansible deployment)

Overcloud Hardware
------------------

The configuration assumes the use of Graphcore reference systems for deployment, and so there are references to specific BIOS configurations, PCI addresses and NVMe drives within this repo.   If you are using alternative systems, you will need to bear this is mind for your reference.   

Compute: 
* Dell R6525
    * AMD EPYC 7742
    * 512GB RAM
    * Mellanox ConnectX-5 100GbE Dual Port NIC - installed in slot 2
    * 7x NVMe devices
    * Firmware:
        * BIOS: 2.3.6 
        * iDRAC: 5.00.10.20 
        * Mellanox:  16.31.10.14


* Controller: Dell R640
    * Intel Xeon Platinum 8168
    * 768GB RAM
    * Mellanox ConnectX-5 100GbE Dual Port NIC - installed in slot 
    * 7x NVMe devices
    * Firmware:
        * BIOS: 2.12.2  
        * iDRAC: 5.00.10.20 
        * Mellanox:  16.31.10.14

* 'Ceph Compute: Dell R640
    * Intel Xeon Platinum 8168
    * 768GB RAM
    * Mellanox ConnectX-5 100GbE Dual Port NIC - installed in slot 
    * 7x NVMe devices
    * Firmware:
        * BIOS: 2.12.2  
        * iDRAC: 5.00.10.20 
        * Mellanox:  16.31.10.14

ToR Switches:
100GbE: Arista DCS-7060CX-32S
1GbE: Arista DCS-7010T-48