# Custom Playbook Notes

## Dell Server Profile

We use the iDRAC server profile to apply BIOS settings.
It all builds on this ansible collection:
https://docs.ansible.com/ansible/latest/collections/dellemc/openmanage/index.html

It takes care to ensure that the profile applies in an
idempontent way. Also beware that sometimes an iDRAC reset
is required for all the BIOS changes to be recognised.

## Dell Firmware Update Docs

We make use of the Dell EMC Repository Manager (DRM):
https://www.dell.com/support/kbdoc/en-uk/000177083/support-for-dell-emc-repository-manager-drm

There is a version for Linux that we make use of via Docker:
https://dl.dell.com/FOLDER07638557M/1/DRMInstaller_3.3.2.735.bin

To run DRM in a container, first start a container,
that has a docker volume to host all the firmware:
```
mkdir -p /home/stack/dell_firmware
podman run --detach -v /home/stack/dell_firmware:/dell_firmware --name dell-drm --restart always centos:8 sleep infinity
```

Copy in then run the installer:
```
podman cp DRMInstaller_3.3.2.735.bin dell-drm:/root/
podman exec -it dell-drm bash
cd /root
chmod +x DRMInstaller_3.3.2.735.bin
./DRMInstaller_3.3.2.735.bin
```

Now you can run DRM, and download a new repo:
```
/opt/dell/dellemcrepositorymanager/DRM_Service.sh &
drm --create -r=idrac_repo_joint --inputplatformlist=R640,R6525
drm --deployment-type=share --location=/dell_firmware -r=idrac_repo_joint
```

Note: sometimes the create call had to be run mutlple times before it worked, with errors relating to Unknown platform: R6525.   Restaring the service might be required.

Now we have the all the files in the docker volume, we can start apache
to expose the repo:
```
sudo podman run -d --name dell-drm-web -p 80:80 -v /home/stack/dell_firmware/:/usr/local/apache2/htdocs/ docker.io/library/httpd:2.4
```

Note: we must use port 80, so we run with sudo such that podman is able to bind port 80.

## Updating the Repo
At a later date we will want to re-baseline to a new version.  The repo can be updated:
```
podman exec -it dell-drm bash
drm --update -r=idrac_repo_joint
# check that it has iterated to a new version
[root@c78921e27b04 dell_firmware]# drm -li=rep

Listing Repositories...

Name                   Latest version   Size      Last modified date
----                   --------------   ----      -------------
idrac_repo_join   1.02             8.64 GB   1/31/22 5:42 P.M

# share the new version
drm --deployment-type=share --location=/dell_firmware -r=idrac_repo_joint:1.02

ls -ltra
-rw-r--r--   1 root root  12038084 Jan 31 17:56 idrac_repo_joint_new_1.02_Catalog.xml
```
The update the dell_drm_repo variable in drac-firmware-update.yml
